A toy Rust baremetal application targeting the dual ARM Cortex-R5 cores in the
Xilinx ZynqMP.

- [Writing an OS in Rust](https://os.phil-opp.com/)
- [Rust Platform
  Support](https://forge.rust-lang.org/release/platform-support.html)
- [Rustup armv7r-none-eabihf
  support](https://rust-lang.github.io/rustup-components-history/armv7r-none-eabihf.html)
